// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_2_1_4GameMode.h"
#include "TDS_2_1_4PlayerController.h"
#include "../Character/TDS_2_1_4Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_2_1_4GameMode::ATDS_2_1_4GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_2_1_4PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}